#!/bin/bash

curl 139.18.211.135:5001/catalog.ttl > catalog.ttl
curl http://139.18.211.135:5001/dataset/49b7432c-8cbb-461b-8db1-d75a1bfcd747/resource/9b28016f-58bc-4662-b4e3-e67a05e52b2d/download/output.ttl > dataset.ttl
cp -f /home/beavis/repositories/continuous-casting/files/process.ttl process.ttl

riot --formatted=turtle dataset.ttl catalog.ttl process.ttl > graph.ttl
