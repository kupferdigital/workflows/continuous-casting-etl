dataSourceFileChannel = Channel.from(params.dataSourceFile).collect()
mappingFileChannel = Channel.from(params.mappingFile).collect()

process mapToRdf {
 
    /* container params.registry + '/rml:latest' */
    container 'rml:latest'

    publishDir 'output', mode: 'copy'

    input:
    path data from dataSourceFileChannel
    path 'mapping.yaml' from mappingFileChannel

    output:
    path 'output.ttl' into output
    
    """
    mv ${data} data
    yarrrml-parser -i mapping.yaml -e dataset_url="http://139.18.211.135:5001/dataset/cccomlog" -o mapping.rml.ttl
    rml-mapper -s turtle --mapping mapping.rml.ttl > output.ttl
    """

}
